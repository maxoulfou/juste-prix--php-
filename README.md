# Juste Prix -PHP-

- [x] Codes sources PHP
- [x] Fichiers sources
- [ ] Codes PHP commentés

> Pour faire fonctionner le code il suffit de lancer WAMP, et de glisser tous les fichiers de ce projet dans un dossier à l'intérieur même du répertoire 'www'.


<img src = "https://gitlab.com/maxoulfou/juste-prix--php-/raw/master/Images/juste_prix.PNG" title = "Screenshot" alt = "Jeu du juste prix">
© Maxence Brochier 2018 - 2019